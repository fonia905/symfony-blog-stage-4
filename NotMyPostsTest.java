import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class NotMyPostsTest extends AuthorizationTest{

    @Test
    @Tag("testPassed")
    void SwitchNotMyPost() throws InterruptedException {
        validTest();
        new WebDriverWait(getDriver(), Duration.ofSeconds(10)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/"));

        WebElement notMyPost = getDriver().findElement(By.xpath(".//button[@role='switch'][@aria-checked='false']"));
        notMyPost.click();
        Thread.sleep(5000);
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//button[@role='switch'][@aria-checked='true']")), "Кнопка не переключена");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//a[@class='post svelte-127jg4t']")), "Посты не найдены");
    }

    @Test
    @Tag("testPassed")
    void OpenNotMyPost() throws InterruptedException {
        SwitchNotMyPost();

        WebElement post = getDriver().findElement(By.xpath(".//a[@class='post svelte-127jg4t']"));
        post.click();
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//h1")), "Заголовок не найден");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//div[@class='item-image']")), "Картинка не найдена");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath("//*[@id=\"app\"]/main/div/div[1]/div/div[3]")), "Подпись не найдена");
    }

    @Test
    @Tag("testFailed")
    void NextPageNotMyPost() throws InterruptedException {
        SwitchNotMyPost();

        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//a[@href='/posts/1']")),"Пост не найден"); //пост для проверки
        WebElement nextPage = getDriver().findElement(By.xpath(".//a[@href='/?page=2&owner=notMe']"));
        nextPage.click();
        Thread.sleep(5000);
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//a[@href='/posts/16']")),"Пост не найден");
        WebElement post = getDriver().findElement(By.xpath(".//a[@class='post svelte-127jg4t']"));
        post.click();
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//h1")), "Заголовок не найден");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//div[@class='item-image']")), "Картинка не найдена");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath("//*[@id=\"app\"]/main/div/div[1]/div/div[3]")), "Подпись не найдена");
    }

    @Test
    @Tag("testPassed")
    void SortOlderNotMyPost() throws InterruptedException {
        SwitchNotMyPost();

        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//a[@href='/posts/1']")),"Пост не найден"); //пост для проверки
        WebElement oldestPage = getDriver().findElement(By.xpath(".//button[@aria-pressed='false']"));
        oldestPage.click();

        Thread.sleep(5000);
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//.//button[@aria-pressed='true']")), "Кнопка не переключена");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//a[@href='/posts/15177']")),"Пост не найден");
        WebElement post = getDriver().findElement(By.xpath(".//a[@class='post svelte-127jg4t']"));
        post.click();
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//h1")), "Заголовок не найден");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//div[@class='item-image']")), "Картинка не найдена");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath("//*[@id=\"app\"]/main/div/div[1]/div/div[3]")), "Подпись не найдена");
    }
}
