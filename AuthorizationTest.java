import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.time.Duration;

public class AuthorizationTest extends AbstractTest {
    @Test
    @Tag("testPassed")
    void NoValidTest() throws InterruptedException {
        new WebDriverWait(getDriver(), Duration.ofSeconds(10)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/login"));

        Actions entry = new Actions(getDriver());

        entry.sendKeys(getDriver().findElement(By.xpath(".//input[@type='text']")),"")
                .sendKeys(getDriver().findElement(By.xpath(".//input[@type='password']")),"")
                .click(getDriver().findElement(By.xpath(".//button")))
                .build()
                .perform();

        Thread.sleep(1000);
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//div[@class='error-block svelte-uwkxn9']")), "Авторизация успешна");
    }

    @Test
    @Tag("testPassed")
    void validTest() throws InterruptedException {
        new WebDriverWait(getDriver(), Duration.ofSeconds(10)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/login"));

        Actions entry = new Actions(getDriver());

        entry.sendKeys(getDriver().findElement(By.xpath(".//input[@type='text']")),"Dima")
                .sendKeys(getDriver().findElement(By.xpath(".//input[@type='password']")),"4085c6bd95")
                .click(getDriver().findElement(By.xpath(".//button")))
                .build()
                .perform();

        Thread.sleep(5000);
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//a[@href='/']")), "Авторизация не успешна");
    }

}
