import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;


public class MyPostsTest extends AuthorizationTest{
    @Test
    @Tag("testPassed")
    void OpenPost() throws InterruptedException {
        validTest();
        new WebDriverWait(getDriver(), Duration.ofSeconds(10)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/"));

        WebElement post = getDriver().findElement(By.xpath(".//a[@class='post svelte-127jg4t']"));
        post.click();
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//h1")), "Заголовок не найден");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//div[@class='item-image']")), "Картинка не найдена");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath("//*[@id=\"app\"]/main/div/div[1]/div/div[3]")), "Подпись не найдена");
    }

    @Test
    @Tag("testPassed")
    void NextPage() throws InterruptedException {
        validTest();
        new WebDriverWait(getDriver(), Duration.ofSeconds(10)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/"));

        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//a[@href='/posts/14979']")),"Пост не найден"); //пост для проверки
        WebElement nextPage = getDriver().findElement(By.xpath(".//a[@href='/?page=2']"));
        nextPage.click();
        Thread.sleep(5000);
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//a[@href='/posts/14974']")),"Пост не найден");
        WebElement post = getDriver().findElement(By.xpath(".//a[@class='post svelte-127jg4t']"));
        post.click();
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//h1")), "Заголовок не найден");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//div[@class='item-image']")), "Картинка не найдена");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath("//*[@id=\"app\"]/main/div/div[1]/div/div[3]")), "Подпись не найдена");
    }

    @Test
    @Tag("testFailed")
    void SortOlder() throws InterruptedException {
        validTest();
        new WebDriverWait(getDriver(), Duration.ofSeconds(10)).until(ExpectedConditions.urlContains("https://test-stand.gb.ru/"));

        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//a[@href='/posts/14979']")),"Пост не найден"); //пост для проверки
        WebElement oldestPage = getDriver().findElement(By.xpath(".//i[@class='material-icons rotate-180 mdc-icon-button__icon']"));
        oldestPage.click();
        Thread.sleep(5000);
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//a[@href='/posts/14974']")),"Пост не найден");
        WebElement post = getDriver().findElement(By.xpath(".//a[@class='post svelte-127jg4t']"));
        post.click();
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//h1")), "Заголовок не найден");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath(".//div[@class='item-image']")), "Картинка не найдена");
        Assertions.assertDoesNotThrow(() ->getDriver().findElement(By.xpath("//*[@id=\"app\"]/main/div/div[1]/div/div[3]")), "Подпись не найдена");
    }
}
